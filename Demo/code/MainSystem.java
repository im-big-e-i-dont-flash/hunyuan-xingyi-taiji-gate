
import java.io.*;
import java.util.*;

//学生信息类
public class MainSystem{
    static String name;  //姓名
    static String number;   //学号(账户)
    static String male;     //性别
    static String age;         //年龄
    static String department;  //院系
    static String password;    //密码
    static Map<String,String> course=new HashMap<String,String>();

    //姓名
    public static String getName(){1
        return name;
    }
    public static void setName(String sName){
        name=sName;
    }

    //账户(学号)
    public  static String getNumber(){
        return number;              //返回account
    }
    public  static void setNumber(String sNumber){
        number=sNumber;      //为account赋值
    }

    //性别
    public static String getMale(){
        return male;
    }
    public static void setMale(String sMale){
        male=sMale;
    }

    //年龄
    public static String getAge(){
        return age;
    }
    public static void setAge(String sAge){
        age=sAge;
    }

    //院系
    public static String getDepartment(){
        return department;
    }
    public static void setDepartment(String sDepartment){
        department=sDepartment;
    }

    //密码
    public  static String getPassword(){
        return password;              //返回password
    }
    public  static void setPassword(String sPassword){
        password=sPassword;      //为password赋值
    }

    public  MainSystem()throws Exception{
        Scanner sc=new Scanner(System.in);
        //欢迎语句
        System.out.println("*********************************************************************");
        System.out.println("*************************欢迎来到学生选课系统!***************************");
        System.out.println("**********************************************************************");

        //main System界面
        System.out.println("1.注册  2.登录  3.学生信息(选课结果)  4.选课  5.退选  6.修改密码  0退出");
        System.out.println("请输入需要办理的业务:");
        int n=sc.nextInt();
        while(true){
            //用于循环让用户操作
            switch(n){
                case 1:
                    MainSystem.login();
                    break;    //调用注册方法
                case 2:
					/*if(a==0){
						System.out.println("请先注册!");
					}else{
						MainSystem.loginPassword();
					}*/
                    MainSystem.loginPassword();
                    break;    //调用登陆方法
                case 3:
                    if(b==0){
                        System.out.println("请先登录!");
                    }else{
                        MainSystem.printInformation();
                    }
                    break;    //调用学生信息方法
                case 4:
                    if(b==0){
                        System.out.println("请先登录!");
                    }else{
                        MainSystem.selectCourse();
                    }
                    break;    //调用选课方法
                case 5:
                    if(b==0){
                        System.out.println("请先登录!");
                    }else{
                        MainSystem.noSelectCourse();
                    };
                    break;    //调用退选方法
                case 6:
                    if(b==0){
                        System.out.println("请先登录!");
                    }else{
                        MainSystem.addPassword();
                    }
                    break;    //调用修改密码方法
                case 0:
                    System.out.println("拜拜!");
                    System.exit(-1);    //调用退出
                default:
                    System.out.println("你输入的不正确...");
            }
            System.out.println("1.注册  2.登录  3.学生信息(选课结果)  4.选课  5.退选  6.修改密码  0退出");
            System.out.println("请输入需要办理的业务:");
            n=sc.nextInt();
        }
    }



    static int a=0;//用于判别是否注册的参数

    //注册
    public static void login() throws Exception{
        Scanner sc=new Scanner(System.in);//创建Scanner对象
        BufferedWriter bw = new BufferedWriter(new FileWriter("student.txt",true));//创建Buffer文件流对象

        System.out.print("请输入学号:");
        MainSystem.setNumber(sc.next());
        bw.write(MainSystem.getNumber()+",");//把学号存入到文件去
        System.out.print("请输入姓名:");
        MainSystem.setName(sc.next());
        bw.write(MainSystem.getName()+",");//把姓名存入到文件去
        System.out.print("请输入性别:");
        MainSystem.setMale(sc.next());
        bw.write(MainSystem.getMale()+",");//把性别存入到文件去
        System.out.print("请输入年龄:");
        MainSystem.setAge(sc.next());
        bw.write(MainSystem.getAge()+",");//把年龄存入到文件去
        System.out.print("请输入院系:");
        MainSystem.setDepartment(sc.next());
        bw.write(MainSystem.getDepartment()+",");//把学院存入到文件去
        //输入密码
        System.out.print("请输入密码:");
        MainSystem.setPassword(sc.next());

        System.out.print("请再次输入密码:");
        String password1=sc.next();

        //循环来鉴别两次输入的密码是否相同


        while(true){
            if (MainSystem.getPassword().equals(password1)==false){
                System.out.println("两次输入密码不同!，请重新输入！");

                password1=sc.next();
            }
            else{
                break;
            }
        }
        a=1;
        System.out.println("恭喜你注册成功！！！");
        bw.write(MainSystem.getPassword());//把密码存入到文件中去
        bw.write("\r\n");
        System.out.println("小提示:您的学号即为您的登陆账户!");
        bw.close();
    }



    static int b=0;//用于判别是否登录的参数
    static int c=0;//用于判断登录的第几个账户信息
    //登录
    public static void loginPassword() throws Exception{
        Map map=new LinkedHashMap();//创建Map对象存放账户和密码

        Scanner sc=new Scanner(System.in);
        BufferedReader br = new BufferedReader(new FileReader("student.txt"));//把student文件中数据读取

        String line="";
        while((line=br.readLine())!=null) {
            String [] strArray=line.split(",");
            map.put(strArray[0],strArray[5]);//把账户和密码放到map集合中去

        }
        if(map.isEmpty()) {
            System.out.println("还没有注册信息");
        }else {
            System.out.print("请输入账户登录");
            String number1=sc.next();
            System.out.print("请输入密码登录");
            String password1=sc.next();
            int flag=0;
            while(flag!=3){

                b=0;
                while(true) {
                    c=0;
                    Set keySet=map.keySet();//获取键的集合
                    Iterator it=keySet.iterator();//迭代健的集合
                    while(it.hasNext()==true) {
                        Object key=it.next();//账户
                        Object value=map.get(key);//密码
                        String key1=(String)key;//把账户转化成String类型
                        String value1=(String)value;//把密码转化成String类型
                        c++;
                        //用户进行登录
                        if(number1.equals(key1)==false||password1.equals(value1)==false){
							/*System.out.println("密码/账户输入错误!");
							flag++;
							System.out.print("请重新输入账户:");
							number1=sc.next();
							System.out.print("请重新输入密码:");
							password1=sc.next();
							break;*/
                        }else{
                            System.out.println("登陆成功!");

                            System.out.println("欢迎您使用本系统!");
                            b=1;
                            new MainSystem();
                            break;
                        }
                    }
                    if(b==1) {
                        break;
                    }else {
                        System.out.println("密码/账户输入错误!");
                        flag++;
                        System.out.print("请重新输入账户:");
                        number1=sc.next();
                        System.out.print("请重新输入密码:");
                        password1=sc.next();
                    }

                }


                if(b==1) {
                    break;
                }

                if(flag==3){
                    System.out.println("输入三次错误!");
                    b=0;
                    new MainSystem();
                    break;
                }
            }
        }
    }

    //修改密码
    public static void addPassword()throws Exception{
        Scanner sc=new Scanner(System.in);
        System.out.print("请输入旧密码:");
        int i=0;
        while(i!=999){
            i++;
            String password1=sc.next();
            if(password1.equals(MainSystem.getPassword())==true){
                System.out.print("请输入新密码:");

                MainSystem.setPassword(sc.next());

                System.out.print("请再次输入密码:");
                String password2=sc.next();

                //循环来鉴别两次输入的密码是否相同

                int flag =1;
                while(flag!=3){
                    if (password2.equals(MainSystem.getPassword())==false){
                        System.out.println("两次输入密码不同!，请重新输入！");
                        flag++;
                        password2=sc.next();
                    }
                    else{
                        break;
                    }
                }

                System.out.println("密码修改成功!请重新登陆。");
                MainSystem.loginPassword();
                return;
            }else{
                System.out.print("旧密码输入错误!请重新输入:");
            }
        }
    }

    //学生信息
    public static void printInformation()throws Exception{
        BufferedReader br = new BufferedReader(new FileReader("student.txt"));//把student文件中数据读取
        int i=0;
        String line="";
        while((line=br.readLine())!=null) {
            String [] strArray=line.split(",");
            i++;
            if(i==c) {
                System.out.println("姓名:"+strArray[1]+" "+"学号(账户):"+strArray[0]+" "+"性别:"+strArray[2]+"年龄"+strArray[3]+" "+"院系:"+strArray[4]);
            }
        }



        //for语句用于打印课程
        if(flag==0) {
            System.out.println("你没有所选取的课程!");
        }else {
            System.out.println("您选的课程:");
            Set keySet=course.keySet();//获取键的集合
            Iterator it=keySet.iterator();//创建Iterator对象
            while(it.hasNext()==true){
                Object key=it.next();
                Object value=course.get(key);
                System.out.println(key+":"+value);
            }
        }

    }

    static int flag=0;//课程数量

    //选课
    public static void selectCourse(){
        Scanner sc=new Scanner(System.in);

        while(true) {//用于循环选课
            System.out.println("1.选取课程 0.退出选课");
            int xuhao=sc.nextInt();
            if(xuhao==1) {
                Map<String,String>map=new HashMap<String,String>();//创建Map对象

                map.put("1","数据结构");//存入选课数据
                map.put("2","高等数学");
                map.put("3","大学英语");
                map.put("4","Java基础");
                System.out.println("你可以选取的课程:");
                Set keySet=map.keySet();//获取键的集合
                Iterator it=keySet.iterator();//创建Iterator对象
                while(it.hasNext()==true){
                    Object key=it.next();
                    Object value=map.get(key);
                    System.out.println(key+":"+value);
                }
                System.out.println("请输入你要选取的代码:");
                int x=sc.nextInt();//用户输入所选取的代码
                switch (x){
                    case 1:course.put("1","数据结构");System.out.println("选取成功!");flag++;break;
                    case 2:course.put("2","高等数学");System.out.println("选取成功!");flag++;break;
                    case 3:course.put("3","大学英语");System.out.println("选取成功!");flag++;break;
                    case 4:course.put("4","Java基础");System.out.println("选取成功!");flag++;break;
                    default:System.out.println("您输入的不正确");
                }
            }
            if(xuhao==0) {
                break;
            }
            if(xuhao!=0&&xuhao!=1) {
                System.out.println("输入错误,请从新输入:");
            }

        }

    }

    //退选
    public static void noSelectCourse(){
        Scanner sc=new Scanner(System.in);

        while(true) {
            System.out.println("你的课程数量:"+flag);
            Set keySet=course.keySet();//获取键的集合
            Iterator it=keySet.iterator();//创建Iterator对象
            while(it.hasNext()==true){
                Object key=it.next();
                Object value=course.get(key);
                System.out.println(key+":"+value);
            }

            System.out.println("1.退选课程 0.退出退选");
            int xuhao=sc.nextInt();
            if(xuhao==1) {
                if(flag!=0) {
                    int c=0;
                    while(true) {
                        System.out.println("请输入你要退选的代码:");
                        String x=sc.next();
                        Set entrySet=course.entrySet();
                        Iterator it1=entrySet.iterator();
                        while(it1.hasNext()==true) {
                            Map.Entry entry=(Map.Entry)(it1.next());
                            Object key=entry.getKey();//键
                            Object value=entry.getValue();//值
                            String str=(String)key;//强制转化为String类型
                            if(x.equals(str)==true) {
                                it1.remove();
                                System.out.println("退选成功!");
                                c++;
                                flag--;
                                break;
                            }else {
                                System.out.println("输入错误!");
                                break;
                            }
                        }
                        if(flag==0) {
                            break;
                        }
                        if(c!=0) {
                            break;
                        }
                    }
                }else {
                    System.out.println("没有课程!为你自动跳出。");
                    break;
                }

            }
            if(xuhao==0) {
                break;
            }
            if(xuhao!=0&&xuhao!=1) {
                System.out.println("输入错误,请从新输入:");
            }
        }

    }


    //测试类
    public static void main(String[] args)throws Exception{
        System.out.println("请选择身份登录:");
        System.out.println("1.老师 2.学生 3.管理员 0.退出");
        Scanner scan=new Scanner(System.in);
        int n=scan.nextInt();
        switch(n)
        {
            case 1:
                System.out.println("老师功能尚未完善:");
                break;
            case 2:
                MainSystem ms=new MainSystem();
                break;
            case 3:
                System.out.println("管理员功能尚未完善:");
                break;
        }
    }
}

