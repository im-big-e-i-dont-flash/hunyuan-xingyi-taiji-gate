/*
 Navicat Premium Data Transfer

 Source Server         : MySQL
 Source Server Type    : MySQL
 Source Server Version : 80017
 Source Host           : localhost:3306
 Source Schema         : softwartsystem

 Target Server Type    : MySQL
 Target Server Version : 80017


*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '课程编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '课程名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for giveclass
-- ----------------------------
DROP TABLE IF EXISTS `giveclass`;
CREATE TABLE `giveclass`  (
  `course_id` int(11) NOT NULL COMMENT '课程编号',
  `teacher_id` int(11) NOT NULL COMMENT '教师编号',
  `capacity` int(11) NOT NULL COMMENT '教课人数\r\n',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '教室容量',
  INDEX `teacher_id`(`teacher_id`) USING BTREE,
  INDEX `teacourse_id`(`course_id`) USING BTREE,
  CONSTRAINT `teacher_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `teacourse_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for goclass
-- ----------------------------
DROP TABLE IF EXISTS `goclass`;
CREATE TABLE `goclass`  (
  `course_id` int(11) NOT NULL COMMENT '课程编号',
  `student_id` int(11) NOT NULL COMMENT '学生编号',
  INDEX `stucourse_id`(`course_id`) USING BTREE,
  INDEX `student_id`(`student_id`) USING BTREE,
  CONSTRAINT `stucourse_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(11) NOT NULL COMMENT '学生编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学生账号密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `id` int(11) NOT NULL COMMENT '教师编号',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '教师姓名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '教师登录密码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for `jiaoshifenpei`
-- ----------------------------
DROP TABLE IF EXISTS `jiaoshifenpei`;
CREATE TABLE `jiaoshifenpei` (
  `ID` int(11) NOT NULL auto_increment,
  `dingdanhao` varchar(50) default NULL,
  `kechengbianhao` varchar(50) default NULL,
  `kechengmingcheng` varchar(50) default NULL,
  `kechengleixing` varchar(50) default NULL,
  `shangkeshijian` varchar(50) default NULL,
  `baomingren` varchar(50) default NULL,
  `jiaoshibianhao` varchar(50) default NULL,
  `jiaoshixingming` varchar(50) default NULL,
  `keshihao` varchar(50) default NULL,
  `weizhi` varchar(50) default NULL,
  `beizhu` varchar(255) default NULL,
  `tianjiashijian` varchar(50) default NULL,
  `tianjiaren` varchar(50) default NULL,
  `addtime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `kechengfenlei`
-- ----------------------------
DROP TABLE IF EXISTS `kechengfenlei`;
CREATE TABLE `kechengfenlei` (
  `ID` int(11) NOT NULL auto_increment,
  `kechengleixing` varchar(50) default NULL,
  `addtime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for `kechengxinxi`
-- ----------------------------
DROP TABLE IF EXISTS `kechengxinxi`;
CREATE TABLE `kechengxinxi` (
  `ID` int(11) NOT NULL auto_increment,
  `kechengbianhao` varchar(50) default NULL,
  `kechengmingcheng` varchar(50) default NULL,
  `kechengleixing` varchar(50) default NULL,
  `tupian` varchar(50) default NULL,
  `shangkefangshi` varchar(50) default NULL,
  `shangkeshijian` varchar(255) default NULL,
  `keshi` varchar(50) default NULL,
  `jiage` varchar(50) default NULL,
  `kechengjieshao` longtext,
  `addtime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
